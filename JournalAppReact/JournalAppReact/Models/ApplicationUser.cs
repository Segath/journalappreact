﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JournalAppReact.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<Journal> Journals { get; set; }
    }
}
