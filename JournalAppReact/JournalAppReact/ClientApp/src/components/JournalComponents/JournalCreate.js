﻿import React, { Component } from 'react';
import './JournalCss.css';
import authService from '../api-authorization/AuthorizeService';


export class JournalCreate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Title: null,
            Subtitle: null,
            Body: null,
        }
        this.handleTitleChange = this.handleTitleChange.bind(this)
        this.handleSubtitleChange = this.handleSubtitleChange.bind(this)
        this.handleBodyChange = this.handleBodyChange.bind(this)
        this.handleClick = this.handleClick.bind(this)        
    }

    handleTitleChange(e) {
        const input = e.target.value
        this.setState({ Title: input})
    }

    handleSubtitleChange(e) {
        const input = e.target.value
        this.setState({ Subtitle: input })
    }

    handleBodyChange(e) {
        const input = e.target.value
        this.setState({ Body: input })
    }

    async handleClick() {

        var d = {
            Title: this.state.Title,
            Subtitle: this.state.Subtitle,
            Body: this.state.Body,
            CreatedDate: new Date(),
            ModifiedDate: new Date(),
        }
        if (this.state.Title && this.state.Subtitle && this.state.Body) {
            console.log("object d: " + d)
            const token = await authService.getAccessToken();
            await fetch('api/journal', {
                headers: !token ? {} : {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(d)
            }).then(response => response.json())
                .then(dd => {
                    console.log("Result from POST: " + dd);
                    this.props.callbackGet();
                });
        }
        else {
            alert("All fields must have a valid value.");
        }

        
    }

    render() {
        return (
            <div className="form-row justify-content-center add-journal">
                <div className="col-md-8">
                    <h4>Add Journal</h4>
                    <div>
                        <input id="title" className="form-control" type="text" name="title" placeholder="Title" onChange={this.handleTitleChange} required /> <br />
                        <input id="subtitle" className="form-control" type="text" name="subtitle" placeholder="Subtitle" onChange={this.handleSubtitleChange} required /> <br />
                        <textarea id="body" className="form-control" name="content" placeholder="Content" onChange={this.handleBodyChange} required /> <br />
                    </div>
                    <button className="btn btn-info form-control" onClick={this.handleClick}>Create Journal</button>
                </div>
             </div>
        )
    }
}

export default JournalCreate;