﻿import React, { Component } from 'react';
import authService from '../api-authorization/AuthorizeService'
import * as moment from 'moment';
import './JournalCss.css';

export class JournalEntry extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title_tmp: this.props.data.title,
            subtitle_tmp: this.props.data.subtitle,
            body_tmp: this.props.data.body,
            editFlag: false
        };
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSubtitleChange = this.handleSubtitleChange.bind(this);
        this.handleBodyChange = this.handleBodyChange.bind(this);
        this.updateJournalEntry = this.updateJournalEntry.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.deleteJournalEntry = this.deleteJournalEntry.bind(this);
    }

    componentDidMount() {
        this.setState({
            data: this.props.data,
            title_tmp: this.props.data.title,
            subtitle_tmp: this.props.data.subtitle,
            body_tmp: this.props.data.body
        });
    }

    handleTitleChange(e) {
        const input = e.target.value
        this.setState({ title_tmp: input })
    }

    handleSubtitleChange(e) {
        const input = e.target.value
        this.setState({ subtitle_tmp: input })
    }

    handleBodyChange(e) {
        const input = e.target.value
        this.setState({ body_tmp: input })
    }

    render() {
        if (!this.state.editFlag) {
            return (
                <div className="journal_section mt-2 content justify-content-center">
                    <h2 className="card-header">{this.props.data.title}</h2>
                    <h4>{this.props.data.subtitle}</h4>
                    <p>{this.props.data.body}</p>
                    <button className="btn btn-journal btn-info" onClick={this.handleUpdate}>Update</button>
                    <button className="btn btn-journal btn-danger" onClick={this.deleteJournalEntry}>Delete</button>
                    <div className="date"><small className="">Created Date: {moment(this.props.data.createdDate).format("lll")}
                        &emsp; Modified Date: {moment(this.props.data.modifiedDate).format("lll")}</small></div>
                </div>
            );
        }
        else {
            return (

                <div className="journal_section mt-2 content justify-content-center form-group">
                    <input className="form-control" type="text" name="title_tmp" onChange={this.handleTitleChange} defaultValue={this.state.title_tmp} required /> <br />
                    <input className="form-control" type="text" name="subtitle_tmp" onChange={this.handleSubtitleChange} defaultValue={this.state.subtitle_tmp} required /> <br />
                    <textarea className="form-control" name="body_tmp" onChange={this.handleBodyChange} defaultValue={this.state.body_tmp} required /><br />
                    <button className="btn btn-journal btn-info" onClick={this.handleClick}>Save changes</button>
                    <button className="btn btn-journal btn-danger" onClick={this.handleUpdate}>Cancel</button>
                 </div>
            )
        }
    }

    handleUpdate() {
        this.setState({ editFlag: !this.state.editFlag });

        if (this.state.editFlag === false) {
            this.setState({
                title_tmp: this.props.data.title,
                subtitle_tmp: this.props.data.subtitle,
                body_tmp: this.props.data.body
            });
        }
    }

    handleClick() {
        this.setState({ editFlag: !this.state.editFlag });
        this.updateJournalEntry();
    }


    async updateJournalEntry() {
        const data = {
            Id: this.props.data.id,
            Title: this.state.title_tmp,
            Subtitle: this.state.subtitle_tmp,
            Body: this.state.body_tmp,
            CreatedDate: this.props.data.createdDate,
            ModifiedDate: new Date()
        }

        if (this.state.title_tmp && this.state.subtitle_tmp && this.state.body_tmp) {
            const token = await authService.getAccessToken();
            await fetch('api/journal/' + this.props.data.id, {
                headers: !token ? {} : {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(data)
            }).then(() => {
                this.props.callbackGet();
            });
        }
        else {
            alert("All fields must have a valid value.");
        }
    }

    async deleteJournalEntry() {
        const token = await authService.getAccessToken();
        await fetch('api/journal/' + this.props.data.id, {
            headers: !token ? {} : {
                'Authorization': `Bearer ${token}`
            },
            method: "DELETE"
        }).then(() => {
            this.props.callbackGet();
        });
    }
}

export default JournalEntry;