﻿import React, { Component } from 'react';
import authService from '../components/api-authorization/AuthorizeService'
import JournalCreate from '../components/JournalComponents/JournalCreate'
import JournalEntry from '../components/JournalComponents/JournalEntry'

export class JournalContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            journals: [],
        }
        this.populateJournalEntries = this.populateJournalEntries.bind(this);
    }

    componentDidMount() {
        this.populateJournalEntries();
    }


    render() {
        var journals = [];
        if (this.state.journals.length > 0) {
            journals = this.state.journals.map(jour => (
                <JournalEntry
                    key={jour.id}
                    data={jour}
                    callbackGet={this.populateJournalEntries}
                >
                </JournalEntry>)
            );
        }
        
        return (
            <React.Fragment>
                <h1>Journals</h1>
                <JournalCreate callbackGet={this.populateJournalEntries} />
                <div className="justify-content-center">
                    {journals}
                </div>
            </React.Fragment>
        )
    }

    async populateJournalEntries(){
        const token = await authService.getAccessToken();
        const response = await fetch('api/journal', {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
        }).then(response => response.json())
            .then(dd => {
                this.setState({  journals: dd.reverse()  });
            });
    }
}

export default JournalContainer