﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JournalAppReact.Data.Migrations
{
    public partial class JournalUserForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Journals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Journals_ApplicationUserId",
                table: "Journals",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals");

            migrationBuilder.DropIndex(
                name: "IX_Journals_ApplicationUserId",
                table: "Journals");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Journals");
        }
    }
}
