﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JournalAppReact.Data.Migrations
{
    public partial class FixToFKType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId1",
                table: "Journals");

            migrationBuilder.DropIndex(
                name: "IX_Journals_ApplicationUserId1",
                table: "Journals");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "Journals");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserId",
                table: "Journals",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Journals_ApplicationUserId",
                table: "Journals",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals");

            migrationBuilder.DropIndex(
                name: "IX_Journals_ApplicationUserId",
                table: "Journals");

            migrationBuilder.AlterColumn<int>(
                name: "ApplicationUserId",
                table: "Journals",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "Journals",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Journals_ApplicationUserId1",
                table: "Journals",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId1",
                table: "Journals",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
