﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JournalAppReact.Data.Migrations
{
    public partial class FixOneToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserID",
                table: "Journals");

            migrationBuilder.DropIndex(
                name: "IX_Journals_ApplicationUserID",
                table: "Journals");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserID",
                table: "Journals",
                newName: "ApplicationUserId");

            migrationBuilder.AlterColumn<int>(
                name: "ApplicationUserId",
                table: "Journals",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId1",
                table: "Journals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Journals_ApplicationUserId1",
                table: "Journals",
                column: "ApplicationUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId1",
                table: "Journals",
                column: "ApplicationUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId1",
                table: "Journals");

            migrationBuilder.DropIndex(
                name: "IX_Journals_ApplicationUserId1",
                table: "Journals");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId1",
                table: "Journals");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserId",
                table: "Journals",
                newName: "ApplicationUserID");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicationUserID",
                table: "Journals",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Journals_ApplicationUserID",
                table: "Journals",
                column: "ApplicationUserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserID",
                table: "Journals",
                column: "ApplicationUserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
