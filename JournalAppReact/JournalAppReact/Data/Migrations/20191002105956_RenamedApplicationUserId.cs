﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JournalAppReact.Data.Migrations
{
    public partial class RenamedApplicationUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserId",
                table: "Journals",
                newName: "ApplicationUserID");

            migrationBuilder.RenameIndex(
                name: "IX_Journals_ApplicationUserId",
                table: "Journals",
                newName: "IX_Journals_ApplicationUserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserID",
                table: "Journals",
                column: "ApplicationUserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserID",
                table: "Journals");

            migrationBuilder.RenameColumn(
                name: "ApplicationUserID",
                table: "Journals",
                newName: "ApplicationUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Journals_ApplicationUserID",
                table: "Journals",
                newName: "IX_Journals_ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Journals_AspNetUsers_ApplicationUserId",
                table: "Journals",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
