﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using JournalAppReact.Data;
using JournalAppReact.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Diagnostics;

namespace JournalAppReact.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class JournalController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> userManager;

        public JournalController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Journal>>> GetAllJournals()
        {
            var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            List<Journal> journals = _context.Journals.Where(j => j.ApplicationUser == user).ToList();
            Debug.WriteLine("############################################################################################################");
            Debug.WriteLine(user);
            Debug.WriteLine(journals);
            Debug.WriteLine("############################################################################################################");
            return Ok(journals);

        }

        [HttpGet]
        [Route("/create")]
        public async Task<ActionResult<ApplicationUser>> CreateDummy()
        {
            var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            Journal jour = new Journal
            {
                Title = "Test",
                Subtitle = "Test-subtitle",
                Body = "Test body body body body",
                ApplicationUser = user
            };

            _context.Journals.Add(jour);
            _context.SaveChanges();
            return Ok(user);
        }



        [HttpGet]
        [Route("/getAllJournals")]
        public IActionResult GetLoggedInJournals()
        {
            return Ok(_context.Journals.ToList());
        }

        [HttpGet]
        [Route("/debugUser")]
        public async Task<ActionResult<ApplicationUser>> GetLoggedInUser()
        {
            var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            return Ok(user);
        }


        [HttpGet("{id}")]
        public IEnumerable<Journal> Get(int id)
        {
            return null;
        }

        [HttpPost]
        public async Task<ActionResult<Journal>> AddJournal([FromBody]Journal jour)
        {        
            var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            jour.ApplicationUser = user;
            _context.Journals.Add(jour);
            _context.SaveChanges();
            return CreatedAtAction("Get", jour);
        }

        [HttpPut("{id}")]
        public IActionResult Add(int id, [FromBody]Journal jour)
        {
            if(id == jour.Id)
            {
                _context.Journals.Update(jour);
                _context.SaveChanges();
                return NoContent();
            }
            else
            {
                return BadRequest("Invalid Id");
            }  
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Journal>> DeleteJournal(int id)
        {
            Debug.WriteLine("FWJIEFJWEFUH)WEHFHWEFUIWEHFUIWEHFUWEFHWEUIFHWEUIFHWEUIFHWEUIFHWEFUIHWEFUIHWEFUIWEHFUIWEHFWEUIFHWEUIFHWEUI");
            Debug.WriteLine("FWJIEFJWEFUH)WEHFHWEFUIWEHFUIWEHFUWEFHWEUIFHWEUIFHWEUIFHWEUIFHWEFUIHWEFUIHWEFUIWEHFUIWEHFWEUIFHWEUIFHWEUI");
            Debug.WriteLine(id);
            Debug.WriteLine(_context.Journals.Find(id));
            Debug.WriteLine("FWJIEFJWEFUH)WEHFHWEFUIWEHFUIWEHFUWEFHWEUIFHWEUIFHWEUIFHWEUIFHWEFUIHWEFUIHWEFUIWEHFUIWEHFWEUIFHWEUIFHWEUI");
            Debug.WriteLine("FWJIEFJWEFUH)WEHFHWEFUIWEHFUIWEHFUWEFHWEUIFHWEUIFHWEUIFHWEUIFHWEFUIHWEFUIHWEFUIWEHFUIWEHFWEUIFHWEUIFHWEUI");
            Journal jour = _context.Journals.Find(id);
            var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            if (jour == null)
            {
                return BadRequest();
            }
            if(jour.ApplicationUser.Id != user.Id)
            {
                return BadRequest("User id and journal id not equal");
            }

            _context.Journals.Remove(jour);
            _context.SaveChanges();

            return jour;

        }
    }

}